package common;

import com.google.gson.JsonObject;
import main.Database;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by iHelo on 11.10.2015.
 */
public class Status extends HttpServlet {

    private Database DB;
    //private Connection connection;

    public Status (Database mainConnect)
    {
        DB = mainConnect;
    }

    @Override
    public void doGet (HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        JsonObject result = new JsonObject();
        Statement state = null;

        String countUser    = "select count(*) as tableCount from User";
        String countThread  = "select count(*) as tableCount from Thread";
        String countForum   = "select count(*) as tableCount from Forum";
        String countPost    = "select count(*) as tableCount from Post";

        try(Connection connection = DB.getConnection();) {

            state = connection.createStatement();
            result.addProperty("code", 0);

            JsonObject responseJS = new JsonObject();
            result.add("response", responseJS);


            ResultSet rs = state.executeQuery(countUser);
            rs.next();
            responseJS.addProperty("user", rs.getInt("tableCount"));

            rs = state.executeQuery(countThread);
            rs.next();
            responseJS.addProperty("thread", rs.getInt("tableCount"));

            rs = state.executeQuery(countForum);
            rs.next();
            responseJS.addProperty("forum", rs.getInt("tableCount"));

            rs = state.executeQuery(countPost);
            rs.next();
            responseJS.addProperty("post",  rs.getInt("tableCount"));

            response.getWriter().println(result);
            connection.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
