package common;

import com.google.gson.JsonObject;
import main.Database;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by iHelo on 11.10.2015.
 */
public class Clear extends HttpServlet {

    private Database DB;
   // private Connection connection;

    public Clear (Database mainConnect)
    {
        DB = mainConnect;
    }

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        try (Connection connection = DB.getConnection();) {

            Statement state = connection.createStatement();
            state.executeUpdate("SET foreign_key_checks = 0;");
            state.executeUpdate(
                    "TRUNCATE `ForumDBTest1`.`Follow`;"
            );
            state.executeUpdate(
                    "TRUNCATE `ForumDBTest1`.`Subscriptions`;"
            );
            state.executeUpdate(
                    "TRUNCATE `ForumDBTest1`.`User`;"
            );
            state.executeUpdate(
                    "TRUNCATE `ForumDBTest1`.`Post`;"
            );
            state.executeUpdate(
                    "TRUNCATE `ForumDBTest1`.`Forum`;"
            );
            state.executeUpdate(
                    "TRUNCATE `ForumDBTest1`.`Thread`;"
            );
            state.executeUpdate(
                    "TRUNCATE `ForumDBTest1`.`UserForum`;"
            );
            state.executeUpdate("SET foreign_key_checks = 1;");
            JsonObject result = new JsonObject();
            result.addProperty("code", 0);
            result.addProperty("response", "OK");
            response.getWriter().println(result);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
