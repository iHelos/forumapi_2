package main;

import com.sun.istack.internal.NotNull;
import forum.CreateForum;
import forum.DetailsForum;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.sql.*;

import common.*;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import post.*;
import sun.nio.ch.ThreadPool;
import thread.*;
import user.*;

import javax.servlet.Servlet;
import javax.sql.PooledConnection;

/**
 * Created by iHelo on 11.10.2015.
 */
public class Main {
    public static final int  STANDARTPORT = 8080;
    public static final String PATH = "/db/api/";

    public static void main(@NotNull String[] args) throws Exception {
        int port = STANDARTPORT ;
        if (args.length == 1) {
            String portString = args[0];
            assert portString != null;
            port = Integer.valueOf(portString);
        }

        Database connection = null;

        try {
            connection = Database.getInstance();
        } catch (Exception e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

        // Common servlets
        Servlet commonClear = new Clear(connection);
        Servlet commonStatus = new Status(connection);

        // User
        Servlet createUser = new CreateUser(connection);
        Servlet followUser = new FollowUser(connection);
        Servlet unfollowUser = new UnfollowUser(connection);
        Servlet detailsUser = new DetailsUser(connection);
        Servlet listFollowers = new listFollowers(connection);
        Servlet listFollowees = new listFollowee(connection);
        Servlet updateUser = new Update(connection);
        Servlet userListPosts = new user.ListPosts(connection);

        //Forum
        Servlet createForum = new CreateForum(connection);
        Servlet detailsForum = new DetailsForum(connection);
        Servlet forumListPosts = new forum.ListPosts(connection);
        Servlet forumListUsers = new forum.ListUsers(connection);
        Servlet forumListThreads = new forum.ListThreads(connection);

        //Thread
        Servlet createThread = new CreateThread(connection);
        Servlet Subscribe = new Subscribe(connection);
        Servlet Unsubscribe = new Unsubscribe(connection);
        Servlet detailsThread = new DetailsThread(connection);
        Servlet Close = new ThreadClose(connection);
        Servlet Open = new ThreadOpen(connection);
        Servlet deleteThread = new ThreadDelete(connection);
        Servlet restoreThread = new ThreadRestore(connection);
        Servlet voteThread = new ThreadVote(connection);
        Servlet updateThread = new ThreadUpdate(connection);
        Servlet threadListPosts = new thread.ListPosts(connection);
        Servlet threadList = new thread.List(connection);

        //Post
        Servlet createPost = new CreatePost(connection);
        Servlet detailsPost = new DetailsPost(connection);
        Servlet deletePost = new PostDelete(connection);
        Servlet restorePost = new PostRestore(connection);
        Servlet votePost = new PostVote(connection);
        Servlet updatePost = new PostUpdate(connection);
        Servlet listPosts = new post.List(connection);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(commonClear), PATH + "clear/");
        context.addServlet(new ServletHolder(commonStatus), PATH + "status/");
        context.addServlet(new ServletHolder(createUser), PATH + "user/create/");
        context.addServlet(new ServletHolder(followUser), PATH + "user/follow/");
        context.addServlet(new ServletHolder(unfollowUser), PATH + "user/unfollow/");
        context.addServlet(new ServletHolder(detailsUser), PATH + "user/details/");
        context.addServlet(new ServletHolder(listFollowers), PATH + "user/listFollowers/");
        context.addServlet(new ServletHolder(listFollowees), PATH + "user/listFollowing/");
        context.addServlet(new ServletHolder(updateUser), PATH + "user/updateProfile/");
        context.addServlet(new ServletHolder(userListPosts), PATH + "user/listPosts/");

        context.addServlet(new ServletHolder(createForum), PATH + "forum/create/");
        context.addServlet(new ServletHolder(detailsForum), PATH + "forum/details/");
        context.addServlet(new ServletHolder(forumListPosts), PATH + "forum/listPosts/");
        context.addServlet(new ServletHolder(forumListUsers), PATH + "forum/listUsers/");
        context.addServlet(new ServletHolder(forumListThreads), PATH + "forum/listThreads/");

        context.addServlet(new ServletHolder(createThread), PATH + "thread/create/");
        context.addServlet(new ServletHolder(detailsThread), PATH + "thread/details/");
        context.addServlet(new ServletHolder(Subscribe), PATH + "thread/subscribe/");
        context.addServlet(new ServletHolder(Unsubscribe), PATH + "thread/unsubscribe/");
        context.addServlet(new ServletHolder(Close), PATH + "thread/close/");
        context.addServlet(new ServletHolder(Open), PATH + "thread/open/");
        context.addServlet(new ServletHolder(deleteThread), PATH + "thread/remove/");
        context.addServlet(new ServletHolder(restoreThread), PATH + "thread/restore/");
        context.addServlet(new ServletHolder(voteThread), PATH + "thread/vote/");
        context.addServlet(new ServletHolder(updateThread), PATH + "thread/update/");
        context.addServlet(new ServletHolder(threadListPosts), PATH + "thread/listPosts/");
        context.addServlet(new ServletHolder(threadList), PATH + "thread/list/");

        context.addServlet(new ServletHolder(createPost), PATH + "post/create/");
        context.addServlet(new ServletHolder(detailsPost), PATH + "post/details/");
        context.addServlet(new ServletHolder(deletePost), PATH + "post/remove/");
        context.addServlet(new ServletHolder(restorePost), PATH + "post/restore/");
        context.addServlet(new ServletHolder(votePost), PATH + "post/vote/");
        context.addServlet(new ServletHolder(updatePost), PATH + "post/update/");
        context.addServlet(new ServletHolder(listPosts), PATH + "post/list/");




        Server server = new Server(port);
       // QueuedThreadPool threadPool = server.getBean(QueuedThreadPool.class);
       // threadPool.setMaxThreads(1000);

        server.setHandler(context);

        server.start();
        server.join();
    }
}
