package main;

import com.google.gson.JsonObject;

/**
 * Created by olegermakov on 12.10.15.
 */
public class ErrorMsg {
    public static JsonObject ReturnMsg(int Code)
    {
        JsonObject result = new JsonObject();
        result.addProperty("code", Code);
        String msg = null;
        switch (Code){
            case 1:
                msg = "no such object";
                break;
            case 2:
                msg = "invalid query";
                break;
            case 3:
                msg = "uncorrect query";
                break;
            case 4:
                msg = "HOLY SHIT";
                break;
            case 5:
                msg = "such object already created";
                break;
        }
        result.addProperty("response", msg);
        return result;
    }
}
