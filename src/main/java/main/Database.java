package main;


import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;

/**
 * Created by olegermakov on 18.10.15.
 */
public class Database {

    private static Database     datasource;
    private BasicDataSource ds;

    private Database() throws IOException, SQLException, PropertyVetoException {
        ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUsername("root");
        ds.setPassword("12345");
        ds.setUrl("jdbc:mysql://localhost:3306/forumdbtest1?autoreconnect=true&useUnicode=yes&characterEncoding=UTF-8");
        // the settings below are optional -- dbcp can work with defaults
        ds.setMinIdle(0);
        ds.setMaxIdle(20);
       // ds.setMaxOpenPreparedStatements(1000000);
        ds.setMaxActive(40);
        //ds.setTestOnBorrow(true);
        //ds.setValidationQuery("Select 1");
       // ds.setRemoveAbandoned(false);
       // ds.setTestOnReturn(true);

    }

    public static Database getInstance() throws IOException, SQLException, PropertyVetoException {
        if (datasource == null) {
            datasource = new Database();
            return datasource;
        } else {
            return datasource;
        }
    }

    public Connection getConnection() throws SQLException {
        return this.ds.getConnection();
    }

}