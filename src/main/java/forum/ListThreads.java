package forum;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class ListThreads extends HttpServlet {
   private Database DB;
   //private Connection connection;

    public ListThreads (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;
    private static String query;
    private static String queryCount = "SELECT count(*) FROM Post WHERE ThreadID = ? and isDeleted=false and isDeleted_thread = false";



    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");

        String short_name = request.getParameter("forum");
        query = "SELECT * FROM Thread where forum_short = ?";

        if (short_name == null || short_name.equals("")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String since_id_string = request.getParameter("since");
        if (since_id_string != null && !since_id_string.equals("")) {
            query += " and date >= '" + since_id_string + "'";
        }

        String order = request.getParameter("order");
        if (order == null || order.equals(""))
            order = "desc";
        else if (!order.equals("desc") && !order.equals("asc")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        query += " order by date " + order;

        String limit_string = request.getParameter("limit");
        if (limit_string != null && !limit_string.equals(""))
            query += " limit " + limit_string;

        String[] relateds = request.getParameterValues("related");
        Boolean markUser = false;
        Boolean markForum = false;

        if (relateds != null) {
            for (int i = 0; i < relateds.length; i++) {
                if (relateds[i].equals("user"))
                    markUser = true;
                else if (relateds[i].equals("forum"))
                    markForum = true;
                else {
                    response.getWriter().println(ErrorMsg.ReturnMsg(3));
                    return;
                }
            }
        }

        try (Connection connection = DB.getConnection();) {
            PreparedStatement state = connection.prepareStatement(query);

            state.setString(1, short_name);
            ResultSet result = state.executeQuery();
            JsonArray arrayFollowers = new JsonArray();
            while (result.next()) {
                JsonObject user = new JsonObject();

                int dislikes = result.getInt("dislikes");
                int likes = result.getInt("likes");
                int points = likes - dislikes;

                user.addProperty("date", result.getString("date").substring(0, 19));
                user.addProperty("dislikes", dislikes);
                user.addProperty("likes", likes);
                user.addProperty("points", points);
                user.addProperty("id", result.getInt("ID_thread"));
                user.addProperty("isClosed", result.getBoolean("isClosed"));
                user.addProperty("isDeleted", result.getBoolean("isDeleted"));
                user.addProperty("message", result.getString("message"));
                user.addProperty("slug", result.getString("slug"));
                user.addProperty("title", result.getString("title"));

                state = connection.prepareStatement(queryCount);
                state.setInt(1, result.getInt("ID_thread"));
                ResultSet rs = state.executeQuery();

                rs.next();
                user.addProperty("posts", rs.getInt(1));



                if(markUser) {
                    JsonObject rss = DetailsUser.getDetails(result.getInt("User_ID"), connection);
                    user.add("user", rss);
                }
                else
                    user.addProperty("user", result.getString("user_email"));

                if(markForum)
                    user.add("forum", DetailsForum.getDetails(result.getString("forum_short"),false,connection));
                else
                    user.addProperty("forum", result.getString("forum_short"));

                arrayFollowers.add(user);
            }

            //connection.close();
            JsonObject resultJSON = new JsonObject();
            resultJSON.addProperty("code",0);
            resultJSON.add("response", arrayFollowers);
            response.getWriter().println(resultJSON);
        }
        catch (SQLException e)
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

    }
}