package forum;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.org.apache.xpath.internal.operations.Bool;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.DetailEntry;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 15.10.15.
 */
public class DetailsForum extends HttpServlet {
    @NotNull
    private static Database DB;
  //  @NotNull
   // private Connection connection;

    public DetailsForum(Database mainConnect) {
        DB = mainConnect;
    }
    // private static PreparedStatement state = null;

    private static String query = "SELECT * FROM Forum WHERE short_name = ?";
    private static String query_int = "SELECT * FROM Forum WHERE ID_forum = ?";
    private static String query_name = "SELECT * FROM Forum WHERE name = ?";

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String short_name = request.getParameter("forum");
        if (short_name == null || short_name == "") {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String user = request.getParameter("related");
        Boolean mark = false;
        if (user != null && user.equals("user")) {
            mark = true;
        }

        JsonObject result = new JsonObject();
        result.addProperty("code", 0);
        try (Connection c = DB.getConnection()) {
            JsonObject responseJSON = getDetails(short_name, mark, c);
            if (responseJSON == null) {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
                return;
            }
            result.add("response", responseJSON);
            response.getWriter().println(result);
            //c.close();
        } catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
            return;
        }

    }

    public static JsonObject getDetails(String short_name, Boolean User, Connection con) throws SQLException {
        // con = DB.getConnection();
        PreparedStatement state = con.prepareStatement(query);

        state.setString(1, short_name);
        ResultSet result = state.executeQuery();
        return commonGetDetails(result, User, con);
    }

    public static JsonObject getDetails(String name, boolean isName, Boolean User, Connection con) throws SQLException {
        // con = DB.getConnection();
        PreparedStatement state = con.prepareStatement(query_name);
        state.setString(1, name);
        ResultSet result = state.executeQuery();
        return commonGetDetails(result, User, con);
    }



    public static JsonObject getDetails(int short_name, Boolean User, Connection con) throws SQLException {
        // con = DB.getConnection();
        PreparedStatement state = con.prepareStatement(query_int);
        state.setInt(1, short_name);
        ResultSet result = state.executeQuery();
        return commonGetDetails(result, User, con);
    }

    private static JsonObject commonGetDetails(ResultSet result, Boolean User, Connection con) throws SQLException {
        JsonObject responseJSON = new JsonObject();
        if (result.next()) {
            responseJSON.addProperty("id", result.getInt(1));
            responseJSON.addProperty("name", result.getString(2));
            responseJSON.addProperty("short_name", result.getString(3));

            String email = result.getString(4);

            if (User)
                responseJSON.add("user", DetailsUser.getDetails(email, con));
            else
                responseJSON.addProperty("user", email);
        } else
            responseJSON = null;
        // con.close();
        return responseJSON;
    }
}