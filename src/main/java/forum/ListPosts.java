package forum;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import thread.DetailsThread;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class ListPosts extends HttpServlet {

   private Database DB;
   //private Connection connection;

    public ListPosts (Database mainConnect)
    {
        DB = mainConnect;
    }
    //private PreparedStatement state = null;
    private static String query = "SELECT * FROM ForumDBTest1.Post left join Forum on ID_forum = ForumID ";


    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String short_name = request.getParameter("forum");
        query = "SELECT * FROM ForumDBTest1.Post where short_forum = ?";

        if (short_name == null || short_name.equals("")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String since_id_string = request.getParameter("since");
        if (since_id_string != null && !since_id_string.equals("")) {
            query += " and date >= '" + since_id_string + "'";
        }

        String order = request.getParameter("order");
        if (order == null || order.equals(""))
            order = "desc";
        else if (!order.equals("desc") && !order.equals("asc")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        query += " order by date " + order;

        String limit_string = request.getParameter("limit");
        if(limit_string != null && !limit_string.equals(""))
            query += " limit " + limit_string;

        String[] user = request.getParameterValues("related");
        Boolean markUser = false;
        Boolean markForum = false;
        Boolean markThread = false;

        if(user != null) {
            for (int i = 0; i < user.length; i++) {
                if (user[i].equals("user"))
                    markUser = true;
                else if (user[i].equals("forum"))
                    markForum = true;
                else if (user[i].equals("thread"))
                    markThread = true;
                else {
                    response.getWriter().println(ErrorMsg.ReturnMsg(3));
                    return;
                }
            }
        }


        //-----------------------------------------
        try (
                        Connection connection = DB.getConnection();
                PreparedStatement state = connection.prepareStatement(query)
        ) {
            state.setString(1, short_name);
            ResultSet result = state.executeQuery();
            JsonArray arrayPosts = new JsonArray();
            while(result.next())
            {
                JsonObject responseJSON = new JsonObject();
                int dislikes = result.getInt(3);
                int likes = result.getInt(6);
                int points = likes - dislikes;
                responseJSON.addProperty("id", result.getInt(1));
                responseJSON.addProperty("date", result.getString(2).substring(0, 19));
                responseJSON.addProperty("dislikes", dislikes);
                responseJSON.addProperty("isClosed", result.getBoolean(4));
                responseJSON.addProperty("isDeleted", result.getBoolean(5) || result.getBoolean("isDeleted_thread"));
                responseJSON.addProperty("likes", likes);
                responseJSON.addProperty("points", points);
                responseJSON.addProperty("message", result.getString(7));
                responseJSON.addProperty("isSpam", result.getBoolean(8));
                responseJSON.addProperty("isHighlighted", result.getBoolean(9));
                responseJSON.addProperty("isApproved", result.getBoolean(11));
                responseJSON.addProperty("isEdited", result.getBoolean(16));
                responseJSON.addProperty("thread", result.getInt("ThreadID"));

                if(markForum) {
                    JsonObject Forum = DetailsForum.getDetails(short_name,false, connection);
                    responseJSON.add("forum", Forum);
                }
                else
                    responseJSON.addProperty("forum", short_name);



                if(markUser)
                {
                    JsonObject User = DetailsUser.getDetails(result.getInt("User"), connection);
                    responseJSON.add("user", User);
                }
                else
                {
                    responseJSON.addProperty("user", result.getString("email_user"));
                }

                if(markThread) {
                    JsonObject Thread = DetailsThread.getDetails(result.getInt("ThreadID"),false,false, connection);
                    responseJSON.add("thread", Thread);
                }
                else
                {
                    responseJSON.addProperty("thread", result.getInt("ThreadID"));
                }

                String Parent = result.getString(12);
                Integer ParentID = null;
                if(Parent!=null)
                    ParentID = Integer.parseInt(Parent);
                responseJSON.addProperty("parent", ParentID);
                arrayPosts.add(responseJSON);
            }

            JsonObject resultJSON = new JsonObject();
            resultJSON.addProperty("code", 0);
            resultJSON.add("response", arrayPosts);
            response.getWriter().println(resultJSON);

        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
        }
    }
}
