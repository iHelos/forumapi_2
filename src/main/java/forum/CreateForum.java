package forum;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 14.10.15.
 */
public class CreateForum extends HttpServlet {

    private Database DB;

    public CreateForum (Database mainConnect)
    {
        DB = mainConnect;
    }

    private String query = "INSERT INTO Forum(email, short_name, name) VALUES (?,?,?)";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");


        JsonObject requestJson = null;
        try {

            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String name = null;
        String short_name = null;
        String email = null;

        try {
            short_name = requestJson.get("short_name").getAsString();
            name = requestJson.get("name").getAsString();
            email = requestJson.get("user").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try(
                Connection con = DB.getConnection();
                PreparedStatement state = con.prepareStatement(query)
        )
        {
            if(DetailsUser.isCreated(email, con)) {
                state.setString(1, email);
                state.setString(2, short_name);
                state.setString(3, name);
                try {
                    state.executeUpdate();
                } catch (MySQLIntegrityConstraintViolationException ignore) {
                    ignore.printStackTrace();
                    // return;
                }
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                JsonObject temp = DetailsForum.getDetails(short_name, false, con);
                if (temp == null)
                {
                    System.out.println("looking for name");
                    temp = DetailsForum.getDetails(name,true,false,con);
                }
                result.add("response", temp);

                response.getWriter().println(result);
            }
            else
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
            }

           // con.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
        }
//        catch (NullPointerException e)
//        {
//          //  e.printStackTrace();
//            response.getWriter().println(ErrorMsg.ReturnMsg(4));
//        }
    }
}
