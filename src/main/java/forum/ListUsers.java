package forum;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class ListUsers extends HttpServlet {

    @NotNull private Database DB;
   // @NotNull private Connection connection;

    public ListUsers (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;
//    private static String query = "SELECT distinct User.* FROM User inner join Post on User.ID_user = Post.ID_post Inner join Forum on Post.ForumID = Forum.ID_forum where short_name = ? ";
    private  String query = "SELECT User.* FROM User inner join UserForum on User.ID_user = UserForum.id_user where short_name = ? ";
    private  String getSubscriptions = "select thread_id from subscriptions where subscriptions.user_id = ?";

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        //query = "SELECT DISTINCT User.* FROM User inner join Post on User.ID_user = Post.User Inner join Forum on Post.ForumID = Forum.ID_forum where short_name = ?";
        query = "SELECT User.* FROM User right join UserForum on User.ID_user = UserForum.id_user where short_name = ?";
        String short_name = request.getParameter("forum");
        if (short_name == null || short_name.equals("")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String since_id_string = request.getParameter("since_id");
        if (since_id_string != null && !since_id_string.equals("")) {
            query += " and user.ID_user >= " + since_id_string;
        }

        String order = request.getParameter("order");
        if (order == null || order.equals(""))
            order = "desc";
        else if (!order.equals("desc") && !order.equals("asc")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        query += " order by UserForum.UF_username " + order;

        String limit_string = request.getParameter("limit");
        if(limit_string != null && !limit_string.equals(""))
            query += " limit " + limit_string;

        try (Connection connection = DB.getConnection();) {
            PreparedStatement state = connection.prepareStatement(query);

            state.setString(1, short_name);
            ResultSet result = state.executeQuery();
            JsonArray arrayFollowers = new JsonArray();
            while (result.next()) {
                JsonObject user = new JsonObject();
                user.addProperty("id", result.getInt(1));
                user.addProperty("username", result.getString(2));
                user.addProperty("email", result.getString(3));
                user.addProperty("about", result.getString(4));
                user.addProperty("name", result.getString(5));
                user.addProperty("isAnonymous", result.getBoolean(6));

                state = connection.prepareStatement(getSubscriptions);
                state.setInt(1, result.getInt(1));
                ResultSet rs = state.executeQuery();

                JsonArray idSub = new JsonArray();
                while(rs.next())
                    idSub.add(rs.getInt(1));
                user.add("subscriptions", idSub);

                arrayFollowers.add(user);
            }
            //connection.close();
            JsonObject resultJSON = new JsonObject();
            resultJSON.addProperty("code",0);
            resultJSON.add("response", arrayFollowers);
            response.getWriter().println(resultJSON);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }
    }
}
