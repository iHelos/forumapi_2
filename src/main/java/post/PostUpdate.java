package post;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import thread.DetailsThread;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class PostUpdate extends HttpServlet {
    @NotNull private Database DB;
    //@NotNull private Connection connection;

    public PostUpdate (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;

    private String query = "UPDATE Post Set message = ? where ID_post = ?";


    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");

        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String message = null;
        Integer ID = null;

        try {
            message = requestJson.get("message").getAsString();
            ID = requestJson.get("post").getAsInt();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try (Connection connection = DB.getConnection()) {
            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1, message);
            state.setInt(2, ID);
            int marker = state.executeUpdate();
            if (marker > 0) {
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                JsonObject responseJSON = DetailsPost.getDetails(ID, false, false, false, connection);
                result.add("response", responseJSON);
                response.getWriter().println(result);
            } else {
                response.getWriter().println(ErrorMsg.ReturnMsg(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
