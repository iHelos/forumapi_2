package post;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.sun.org.apache.xpath.internal.operations.Bool;
import forum.DetailsForum;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import thread.DetailsThread;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.nimbus.State;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by olegermakov on 16.10.15.
 * ###Optional
 * parent
 * <p>
 * ```int``` id of parent post. Default: None
 * isApproved
 * <p>
 * ```bool``` is post marked as approved by moderator
 * isHighlighted
 * <p>
 * ```bool``` is post marked as higlighted
 * isEdited
 * <p>
 * ```bool``` is post marked as edited
 * isSpam
 * <p>
 * ```bool``` is post marked as spam
 * isDeleted
 * <p>
 * ```bool``` is post marked as deleted
 * <p>
 * <p>
 * ###Requried
 * date
 * <p>
 * ```str``` date of creation. Format: 'YYYY-MM-DD hh-mm-ss'
 * thread
 * <p>
 * ```int``` thread id of this post
 * message
 * <p>
 * ```str``` post body
 * user
 * <p>
 * ```str``` author email
 * forum
 * <p>
 * ```str``` forum short_name
 */



/*

 */
public class CreatePost extends HttpServlet {

    static private final String SEPARATOR = "/";

    @NotNull
    private Database DB;

    public CreatePost(Database mainConnect) {
        DB = mainConnect;
    }

    // private PreparedStatement state = null;
    //
    private String query = "INSERT INTO Post(date, isDeleted," +
            " message, isSpam, isHighlighted, isApproved, User, ForumID, ThreadID, isEdited, ParentID, partialID, PartialPath, email_user, short_forum) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private String query_helper = "INSERT IGNORE INTO UserForum(id_user, short_name, UF_username) VALUES (?,?,?)";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String dateString = null;
        Integer thread = null;
        String message = null;
        String user = null;
        String forum = null;

        Date date = new Date();

        try {
            thread = requestJson.get("thread").getAsInt();
            message = requestJson.get("message").getAsString();
            user = requestJson.get("user").getAsString();
            forum = requestJson.get("forum").getAsString();
            dateString = requestJson.get("date").getAsString();
            date = SDF.parse(dateString);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        //---------------------------------------------------

        JsonElement isA = requestJson.get("isApproved");
        Boolean isApproved = false;
        if (isA != null)
            isApproved = isA.getAsBoolean();

        JsonElement isH = requestJson.get("isHighlighted");
        Boolean isHighlighted = false;
        if (isH != null)
            isHighlighted = isH.getAsBoolean();

        JsonElement isE = requestJson.get("isEdited");
        Boolean isEdited = false;
        if (isE != null)
            isEdited = isE.getAsBoolean();

        JsonElement isS = requestJson.get("isSpam");
        Boolean isSpam = false;
        if (isS != null)
            isSpam = isS.getAsBoolean();

        JsonElement isD = requestJson.get("isDeleted");
        Boolean isDeleted = false;
        if (isD != null)
            isDeleted = isD.getAsBoolean();

        JsonElement parent = requestJson.get("parent");
        Integer PID = null;
        ResultSet ParentPartial = null;

        Integer ParentPartialID = 0;
        String PartialPath = null;
        String ID_post = null;
        int ChildCount = 0;

        try (Connection connection = DB.getConnection()) {
            JsonObject User = DetailsUser.getDetails(user, connection);
            JsonObject Forum = DetailsForum.getDetails(forum, false, connection);

            if (User == null || Forum == null) {
                response.getWriter().println(ErrorMsg.ReturnMsg(3));
                return;
            }
            if (parent != null && !parent.isJsonNull()) {
                PID = parent.getAsInt();
                ParentPartial = DetailsPost.GetPartial(PID, connection);
                ParentPartialID = ParentPartial.getInt("partialID");
                ChildCount = ParentPartial.getInt("ChildCount");
                PartialPath = ParentPartial.getString("PartialPath");
                ID_post = ParentPartial.getString("ID_post");
                if (ParentPartial == null) {
                    response.getWriter().println(ErrorMsg.ReturnMsg(3));
                    return;
                }

            }

            PreparedStatement state = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            state.setString(1, SDF.format(date));
            state.setBoolean(2, isDeleted);
            state.setString(3, message);
            state.setBoolean(4, isSpam);
            state.setBoolean(5, isHighlighted);
            state.setBoolean(6, isApproved);
            int UserID = User.get("id").getAsInt();
            state.setInt(7, UserID);
            state.setInt(9, thread);
            state.setInt(8, Forum.get("id").getAsInt());
            state.setBoolean(10, isEdited);
            if (PID != null) {
                state.setInt(11, PID);
                state.setInt(12, ChildCount + 1);

                if (ParentPartialID != null && !ParentPartialID.equals(0))
                    state.setString(13, PartialPath + SEPARATOR + leftPad(ParentPartialID.toString(), 3));
                else
                    state.setString(13, leftPad(ID_post, 3));
            } else {
                state.setNull(11, Types.INTEGER);
                state.setInt(12, 0);
                state.setString(13, "");
            }
            state.setString(14, user);
            state.setString(15, forum);
            int marker = state.executeUpdate();
            ResultSet rs = state.getGeneratedKeys();
            rs.next();
            JsonObject result = new JsonObject();
            result.addProperty("code", 0);
            JsonObject responseJSON = DetailsPost.getDetails(rs.getInt(1), false, false, false, connection);
            result.add("response", responseJSON);

            if (PID != null) {
                state = connection.prepareStatement("UPDATE Post SET ChildCount = ChildCount + 1 WHERE ID_post=" + PID);
                state.executeUpdate();
            } else {
                state = connection.prepareStatement("UPDATE Post SET partialID = ID_post WHERE ID_post=" + rs.getInt(1));
                state.executeUpdate();
            }

            state = connection.prepareStatement(query_helper);
            state.setString(2, forum);
            state.setInt(1, UserID);
            String name = null;
            try {
                name = User.get("name").getAsString();
            } catch (UnsupportedOperationException e) {

            }
            state.setString(3, name);
            state.executeUpdate();

            response.getWriter().println(result);

        } catch (SQLException e) {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
            return;
        }


        //-----------------------------------------------------

        //----------------------------------------------------------

    }

    public static String leftPad(String s, int width) {
        return String.format("%" + width + "s", s).replace(' ', '0');
    }

}
