package post;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class PostVote extends HttpServlet {
    @NotNull private Database DB;
  //  @NotNull private Connection connection;

    public PostVote (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;
    private String queryLike = "UPDATE Post SET likes = likes + 1 WHERE ID_post=?";
    private String queryDislike = "UPDATE Post SET dislikes = dislikes + 1 WHERE ID_post=?";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }
        Integer thread = null;
        Integer vote = null;
        try {
            thread = requestJson.get("post").getAsInt();
            vote = requestJson.get("vote").getAsInt();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try (Connection connection = DB.getConnection();) {
            PreparedStatement state;
            if(vote == 1) {
                state = connection.prepareStatement(queryLike);
            }
            else if(vote == -1)
                state = connection.prepareStatement(queryDislike);
            else
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(4));
                return;
            }
            state.setInt(1, thread);
            state.executeUpdate();
            //connection.close();
            JsonObject responseJSON = new JsonObject();
            JsonObject result = new JsonObject();
            result.addProperty("code", 0);
            responseJSON.addProperty("post", thread);
            result.add("response", responseJSON);
            response.getWriter().println(result);
        } catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(5));
        }
    }
}
