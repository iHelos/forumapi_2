package post;

import com.google.gson.JsonObject;
import forum.DetailsForum;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import thread.DetailsThread;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by olegermakov on 17.10.15.
 */
public class DetailsPost extends HttpServlet {

    @NotNull
    private static Database DB;

    public DetailsPost (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private static PreparedStatement state = null;
   // private static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static String query = "SELECT * FROM post WHERE ID_post = ?";

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String IDstring = request.getParameter("post");
        if (IDstring == null || IDstring == "") {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }
        int ID = 0;
        try{
            ID = Integer.parseInt(IDstring);
        }
        catch (Exception e)
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String[] user = request.getParameterValues("related");
        Boolean markUser = false;
        Boolean markForum = false;
        Boolean markThread = false;

        if(user != null) {
            for (int i = 0; i < user.length; i++) {
                if (user[i].equals("user"))
                    markUser = true;
                else if (user[i].equals("forum"))
                    markForum = true;
                else if (user[i].equals("thread"))
                    markThread = true;
                else {
                    response.getWriter().println(ErrorMsg.ReturnMsg(3));
                    return;
                }
            }
        }

        JsonObject result = new JsonObject();
        result.addProperty("code", 0);
        try (Connection connection = DB.getConnection()){
            JsonObject responseJSON = getDetails(ID, markUser, markForum, markThread, connection);
            if (responseJSON == null) {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
                return;
            }
            result.add("response", responseJSON);
            response.setContentType("application/json; charset=utf-8");
            response.getWriter().println(result);
        }
        catch (SQLException e)
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
            return;
        }


    }

    public static JsonObject getDetails(int ID, Boolean User, Boolean Forum, Boolean Thread, Connection connection) throws SQLException {
        //Connection connection = DB.getConnection();
        PreparedStatement state = connection.prepareStatement(query);
        state.setInt(1, ID);
        ResultSet result = state.executeQuery();
        try {
            return commonGetDetails(result, User, Forum, Thread, connection);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    private static JsonObject commonGetDetails(ResultSet result, Boolean User, Boolean Forum, Boolean Thread, Connection con) throws SQLException, ParseException {
        JsonObject responseJSON = new JsonObject();
        if (result.next()) {


            int dislikes = result.getInt(3);
            int likes = result.getInt(6);
            int points = likes - dislikes;

            responseJSON.addProperty("id", result.getInt(1));
            responseJSON.addProperty("date", result.getString(2).substring(0,19));
            responseJSON.addProperty("dislikes", dislikes);
            responseJSON.addProperty("isClosed", result.getBoolean(4));
            responseJSON.addProperty("isDeleted", result.getBoolean(5) || result.getBoolean("isDeleted_thread"));
            responseJSON.addProperty("likes", likes);
            responseJSON.addProperty("message", result.getString(7));
            responseJSON.addProperty("isSpam", result.getBoolean(8));
            responseJSON.addProperty("isHighlighted", result.getBoolean(9));
            responseJSON.addProperty("isApproved", result.getBoolean(11));
            responseJSON.addProperty("isEdited", result.getBoolean(16));
            String Parent = result.getString(12);
            Integer ParentID = null;
            if(Parent!=null)
                ParentID = Integer.parseInt(Parent);
            responseJSON.addProperty("parent", ParentID);
            responseJSON.addProperty("points", points);


            int user = result.getInt(13);
            int threadID = result.getInt(15);

            if(User) {
                JsonObject UserJS = DetailsUser.getDetails(user, con);
                responseJSON.add("user", UserJS);
            }
            else
                responseJSON.addProperty("user", result.getString("email_user"));

            if(Forum) {
                JsonObject ForumJS = DetailsForum.getDetails(result.getInt(14), false,con);
                responseJSON.add("forum", ForumJS);
            }
            else
                responseJSON.addProperty("forum", result.getString("short_forum"));

            if(Thread)
                responseJSON.add("thread", DetailsThread.getDetails(threadID, false, false, con));
            else
                responseJSON.addProperty("thread", threadID);


        } else
            responseJSON = null;
        //con.close();
        return responseJSON;
    }
    public static ResultSet GetPartial(int ID, Connection connection) throws SQLException
    {
        PreparedStatement state = connection.prepareStatement(query);
        state.setInt(1, ID);
        ResultSet result = state.executeQuery();
        if (result.next()) {
            return result;
        }
        return null;
    }
}


