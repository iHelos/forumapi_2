package post;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class PostDelete extends HttpServlet {
    @NotNull private Database DB;
  //  @NotNull private Connection connection;

    public PostDelete (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;
    private String query = "UPDATE Post SET isDeleted = true WHERE ID_post=?";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }
        Integer thread = null;

        try {
            thread = requestJson.get("post").getAsInt();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try (Connection   connection = DB.getConnection();) {
            //connection = DB.getConnection();
            PreparedStatement state = connection.prepareStatement(query);
            state.setInt(1, thread);
            state.executeUpdate();
            JsonObject responseJSON = new JsonObject();
            JsonObject result = new JsonObject();
            result.addProperty("code", 0);
            responseJSON.addProperty("post", thread);
            result.add("response", responseJSON);
        //    connection.close();
            response.getWriter().println(result);
        } catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(5));
        }
    }
}
