package user;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 11.10.15.
 * <p>
 * <p>
 * username
 * str user name
 * <p>
 * about
 * str user info
 * <p>
 * name
 * str user name
 * <p>
 * email
 * str user email
 */
public class CreateUser extends HttpServlet {
    @NotNull
    private Database DB;
    public CreateUser(Database mainConnect) {
        DB = mainConnect;
    }

   // private PreparedStatement state = null;

    private String query = "INSERT INTO USER(username, about, NAME, email, isAnonymous) VALUES (?,?,?,?,?)";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");

        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String username = null;
        String about = null;
        String name = null;
        String email = null;

        JsonElement isAnonymous = requestJson.get("isAnonymous");
        Boolean isAnon = false;
        if (isAnonymous != null)
            isAnon = isAnonymous.getAsBoolean();

        if (!isAnon) {
            try {
                username = requestJson.get("username").getAsString();
                about = requestJson.get("about").getAsString();
                name = requestJson.get("name").getAsString();
                email = requestJson.get("email").getAsString();
            } catch (Exception e) {
                response.getWriter().println(ErrorMsg.ReturnMsg(2));
                return;
            }
        } else {
            try {
                email = requestJson.get("email").getAsString();
              //  name = requestJson.get("name").getAsString();
            } catch (Exception e) {
                response.getWriter().println(ErrorMsg.ReturnMsg(2));
                return;
            }
        }

      //  Connection connection = null;
        try (Connection connection = DB.getConnection()) {
            //connection = DB.getConnection();
           PreparedStatement state = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            state.setString(1, username);
            state.setString(2, about);
            state.setString(3, name);
            state.setString(4, email);
            state.setBoolean(5, isAnon);
            int marker = state.executeUpdate();

            if (marker > 0) {
               /* ResultSet rs = state.getGeneratedKeys();
                if (rs == null || !rs.next()) {
                    state.executeUpdate();
                    rs = state.getGeneratedKeys();
                }*/
                JsonObject result = new JsonObject();
                JsonObject responseJSON = new JsonObject();
                try {
                    result.addProperty("code", 0);
                    responseJSON = DetailsUser.getDetails(email, connection);
                }
                catch (NullPointerException e)
                {
                    connection.close();
                    response.getWriter().println(ErrorMsg.ReturnMsg(5));
                    return;
                }
               /* responseJSON.addProperty("about", about);
                responseJSON.addProperty("email", email);
                responseJSON.addProperty("id", rs.getInt(1));
                responseJSON.addProperty("isAnonymous", isAnon);
                responseJSON.addProperty("name", name);
                responseJSON.addProperty("username", username);*/
                result.add("response", responseJSON);
                response.getWriter().println(result);

            } else {
                response.getWriter().println(ErrorMsg.ReturnMsg(5));
            }
        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(5));
        }
        catch (NullPointerException e)
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
        }

    }
}

