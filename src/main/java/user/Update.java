package user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 14.10.15.
 */
public class Update extends HttpServlet {
    @NotNull private Database DB;
   // @NotNull private Connection connection;

    public Update (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;

    private String query = "UPDATE User Set name = ?, about = ? where email = ?";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");

        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String email = null;
        String about = null;
        String name = null;

        try {
            about = requestJson.get("about").getAsString();
            name = requestJson.get("name").getAsString();
            email = requestJson.get("user").getAsString();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try (Connection connection = DB.getConnection();) {

            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1, name);
            state.setString(2, about);
            state.setString(3, email);
            int marker = state.executeUpdate();
            if (marker > 0) {
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                JsonObject responseJSON = DetailsUser.getDetails(email, connection);
                result.add("response", responseJSON);
                response.getWriter().println(result);
            } else {
                response.getWriter().println(ErrorMsg.ReturnMsg(5));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
