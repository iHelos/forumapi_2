package user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 12.10.15.
 */
public class FollowUser extends HttpServlet {
    private Database DB;
   // private Connection connection;

    public FollowUser (Database mainConnect)
    {
        DB = mainConnect;
    }

   // private PreparedStatement state = null;
    private String query = "INSERT INTO Follow(idFollower, idFollowee, emailFollower, emailFollowee) SELECT Fer.ID_user, Fee.ID_user, Fer.email, Fee.email from User Fer join User Fee where Fer.email = ? AND Fee.email = ?";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String follower = null;
        String followee = null;

        try {
            follower = requestJson.get("follower").getAsString();
            followee = requestJson.get("followee").getAsString();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try (Connection connection = DB.getConnection();) {

            PreparedStatement state = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            state.setString(1, follower);
            state.setString(2, followee);
            int marker = state.executeUpdate();
            if (marker > 0) {
                ResultSet rs = state.getGeneratedKeys();
                rs.next();
                JsonObject responseJSON = DetailsUser.getDetails(follower, connection);
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                result.add("response", responseJSON);
                response.getWriter().println(result);
            }
            else {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
            }
            connection.close();
        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(5));
        }

    }

}
