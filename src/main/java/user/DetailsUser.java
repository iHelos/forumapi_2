package user;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by olegermakov on 13.10.15.
 */
public class DetailsUser extends HttpServlet {
    @NotNull private static Database DB;

    public DetailsUser (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private static PreparedStatement state = null;

    private static String queryID = "select * from User where ID_user = ?";
    private static String queryEmail = "select * from User where email = ?";

    private static String isCreated = "select email from User where email = ?";

    private static String getFollowees = "select emailFollowee from Follow  where idFollower = ?";
    private static String getFollowers = "select emailFollower from Follow  where idFollowee = ?";

    private static String getSubscriptions = "select thread_id from subscriptions where subscriptions.user_id = ?";

    @Override
    public void doGet(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String email = request.getParameter("user");
        if (email == null || email == "") {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }
        else
        {
            try (Connection con = DB.getConnection())
            {
                JsonObject responseJSON = getDetails(email, con);
                if(responseJSON == null)
                {
                    response.getWriter().println(ErrorMsg.ReturnMsg(1));
                    return;
                }

                JsonObject result = new JsonObject();
                result.addProperty("code",0);
                result.add("response", responseJSON);
                response.getWriter().println(result);

            }
            catch (SQLException e)
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(4));
                return;
            }
        }
    }

    public static JsonObject getDetails(int ID, Connection connection) throws SQLException
    {
        //Connection connection = DB.getConnection();
        PreparedStatement state = connection.prepareStatement(queryID);
        state.setInt(1, ID);
        //ResultSet result = state.executeQuery();
        return  commonGetDetails(state, connection);
    }

    public static JsonObject getDetails(String email, Connection connection) throws SQLException
    {
        PreparedStatement state = connection.prepareStatement(queryEmail);
        state.setString(1, email);
        //ResultSet result = state.executeQuery();
        return  commonGetDetails(state, connection);
    }

    private static JsonObject commonGetDetails(PreparedStatement statez, Connection con) throws SQLException
    {
        ResultSet result = statez.executeQuery();
        JsonObject responseJSON = new JsonObject();
        if(result.next()) {
            try {
                responseJSON.addProperty("about", result.getString("about"));
                responseJSON.addProperty("username", result.getString("username"));
                responseJSON.addProperty("name", result.getString("name"));
            }
            catch (NullPointerException e)
            {
                return null;
            }

            int ID = result.getInt("ID_user");
            responseJSON.addProperty("id", ID);
            responseJSON.addProperty("isAnonymous", result.getBoolean("isAnonymous"));
            responseJSON.addProperty("email", result.getString("email"));
            PreparedStatement state = con.prepareStatement(getFollowees);
            state.setInt(1, ID);
            result = state.executeQuery();
            JsonArray emailsFollowing = new JsonArray();
            while(result.next()) {
                emailsFollowing.add(result.getString(1));
            }
            responseJSON.add("following", emailsFollowing);

            state = con.prepareStatement(getFollowers);
            state.setInt(1,ID);
            result = state.executeQuery();
            JsonArray emailsFollowers = new JsonArray();
            while(result.next()) {
                emailsFollowers.add(result.getString(1));
            }
            responseJSON.add("followers", emailsFollowers);

           // state.close();
            state = con.prepareStatement(getSubscriptions);
            state.setInt(1, ID);
            result = state.executeQuery();
            JsonArray idSub = new JsonArray();
            while(result.next())
                idSub.add(result.getInt(1));
            responseJSON.add("subscriptions",idSub);
        }
        else
            responseJSON = null;
       // result.close();
       // state.close();
       // DB.close();
        return  responseJSON;
    }
    public static boolean isCreated(String email, Connection connection)
    {
        try {
            PreparedStatement state = connection.prepareStatement(isCreated);
            state.setString(1, email);
            ResultSet result = state.executeQuery();
            Boolean res = result.next();
            return res;
        }
        catch (SQLException e)
        {
            return false;
        }
    }
}