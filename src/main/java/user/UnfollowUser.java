package user;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 12.10.15.
 */
public class UnfollowUser extends HttpServlet {
    @NotNull private Database DB;
    //@NotNull private Connection connection;

    public UnfollowUser (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;
    private String query = "DELETE Follow FROM Follow left join User Fee on idFollowee = Fee.ID_User left join User Fer on idFollower = Fer.ID_User WHERE Fer.email = ? and Fee.email = ?";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String follower = null;
        String followee = null;

        try {
            follower = requestJson.get("follower").getAsString();
            followee = requestJson.get("followee").getAsString();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }
        try (Connection connection = DB.getConnection();) {

            PreparedStatement state = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            state.setString(1, follower);
            state.setString(2, followee);
            int marker = state.executeUpdate();
            if (marker > 0) {
                ResultSet rs = state.getGeneratedKeys();
                rs.next();
                JsonObject responseJSON = DetailsUser.getDetails(follower,connection);
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                result.add("response", responseJSON);
                response.getWriter().println(result);
            }
            else {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
            }
            connection.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(5));
        }
    }

}
