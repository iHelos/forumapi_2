package user;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 13.10.15.
 */
public class listFollowers extends HttpServlet {

    @NotNull private static Database DB;

    public listFollowers (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;
    private static String query = "select * from followers" +
            " where FolloweeEmail = ? ";

    private static String getSubscriptions = "select thread_id from subscriptions where subscriptions.user_id = ?";

            /*and FolloweeID >= ?  " +
            "order by FolloweeName ";*/


    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String email = request.getParameter("user");
        if (email == null || email.equals("")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        query = "select * from followers" +
                " where FolloweeEmail = ? ";

        String since_id_string = request.getParameter("since_id");
        if (since_id_string != null && !since_id_string.equals("")) {
            try {
                query = query + "and FollowerID >= " + since_id_string;
            }
            catch (NumberFormatException e)
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(2));
                return;
            }
        }

        String order = request.getParameter("order");
        if (order == null || order.equals(""))
            order="desc";
        else if (!order.equals("desc") && !order.equals("asc")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }
        query = query + " order by FollowerName " + order;

        String limit_string = request.getParameter("limit");
        if (limit_string != null && !limit_string.equals("")) {
            query = query + " limit " + limit_string;
        }

        /*------------------------------------*/

        try (Connection connection = DB.getConnection();){

            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1, email);
            ResultSet result = state.executeQuery();
            JsonArray arrayFollowees = null;
            while(result.next())
            {
                if (arrayFollowees == null)
                    arrayFollowees = new JsonArray();
                JsonObject user = new JsonObject();
                String emailFollowee = result.getString(5);
                if (emailFollowee == null)
                    break;
                user.addProperty("id", result.getInt(3));
                user.addProperty("username", result.getString(4));
                user.addProperty("email", emailFollowee);
                user.addProperty("about", result.getString(6));
                user.addProperty("name", result.getString(7));
                user.addProperty("isAnonymous", result.getBoolean(8));
                user.add("following", listFollowee.getFolloweeList(emailFollowee));
                user.add("followers", getFollowersList(emailFollowee));

                state = connection.prepareStatement(getSubscriptions);
                state.setInt(1, result.getInt(3));
                ResultSet Subs = state.executeQuery();
                JsonArray idSub = new JsonArray();
                while(Subs.next())
                    idSub.add(Subs.getInt(1));
                user.add("subscriptions",idSub);

                arrayFollowees.add(user);
            }
            connection.close();
            if(arrayFollowees == null)
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
                return;
            }
            JsonObject resultJSON = new JsonObject();
            resultJSON.addProperty("code",0);
            resultJSON.add("response", arrayFollowees);
            response.getWriter().println(resultJSON);

        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
        }
    }

    public static JsonArray getFollowersList(@NotNull String email) throws SQLException
    {
        query = "select * from followers" +
                " where FolloweeEmail = ? ";
        PreparedStatement stateFollowers = null;
        Connection connection = DB.getConnection();
        stateFollowers = connection.prepareStatement(listFollowers.query);
        stateFollowers.setString(1, email);
        ResultSet result = stateFollowers.executeQuery();
        JsonArray arrayEmail = new JsonArray();
        while(result.next())
        {
            if(result.getString(5)!=null)
            arrayEmail.add(result.getString(5));
        }
        connection.close();
        return arrayEmail;
    }
}