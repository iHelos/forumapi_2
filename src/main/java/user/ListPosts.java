package user;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 17.10.15.
 */
public class ListPosts extends HttpServlet {

    @NotNull private Database DB;
   // @NotNull private Connection connection;

    public ListPosts (Database mainConnect)
    {
        DB = mainConnect;
    }

    private static String query;
            /*and FolloweeID >= ?  " +
            "order by FolloweeName ";*/

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String email = request.getParameter("user");
      //  query = "SELECT * FROM ForumDBTest1.Post left join User on ID_user = User where User.email = ? ";
        query = "SELECT * FROM ForumDBTest1.Post where email_user = ? ";

        String since = null;
        if (email == null || email.equals("")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String since_id_string = request.getParameter("since");
        if (since_id_string != null && !since_id_string.equals("")) {
            query += " and date >= '" + since_id_string + "'";
        }

        String order = request.getParameter("order");
        if (order == null || order.equals(""))
            order = "desc";
        else if (!order.equals("desc") && !order.equals("asc")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        query += " order by date " + order;

        String limit_string = request.getParameter("limit");
        if(limit_string != null && !limit_string.equals(""))
            query += " limit " + limit_string;


        //-----------------------------------------
        try (Connection connection = DB.getConnection();
        PreparedStatement state = connection.prepareStatement(query);
             ) {

            state.setString(1, email);
            ResultSet result = state.executeQuery();
            JsonArray arrayPosts = new JsonArray();
            while(result.next())
            {
                JsonObject responseJSON = new JsonObject();
                int dislikes = result.getInt(3);
                int likes = result.getInt(6);
                int points = likes - dislikes;
                responseJSON.addProperty("id", result.getInt(1));
                responseJSON.addProperty("date", result.getString(2).substring(0, 19));
                responseJSON.addProperty("dislikes", dislikes);
                responseJSON.addProperty("isClosed", result.getBoolean(4));
                responseJSON.addProperty("isDeleted", result.getBoolean(5) || result.getBoolean("isDeleted_thread"));
                responseJSON.addProperty("likes", likes);
                responseJSON.addProperty("points", points);
                responseJSON.addProperty("message", result.getString(7));
                responseJSON.addProperty("isSpam", result.getBoolean(8));
                responseJSON.addProperty("isHighlighted", result.getBoolean(9));
                responseJSON.addProperty("isApproved", result.getBoolean(11));
                responseJSON.addProperty("isEdited", result.getBoolean(16));
                responseJSON.addProperty("thread", result.getInt("ThreadID"));

                responseJSON.addProperty("forum", result.getString("short_forum"));

                responseJSON.addProperty("user", email);

                String Parent = result.getString(12);
                Integer ParentID = null;
                if(Parent!=null)
                    ParentID = Integer.parseInt(Parent);
                responseJSON.addProperty("parent", ParentID);
                arrayPosts.add(responseJSON);
            }
            connection.close();
            JsonObject resultJSON = new JsonObject();
            resultJSON.addProperty("code",0);
            resultJSON.add("response", arrayPosts);
            response.getWriter().println(resultJSON);

        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
        }
    }
}