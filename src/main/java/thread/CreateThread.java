package thread;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import forum.DetailsForum;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by olegermakov on 14.10.15.
 */
public class CreateThread extends HttpServlet {
    @NotNull private Database DB;
   // @NotNull private Connection connection;

    public CreateThread (Database mainConnect)
    {
        DB = mainConnect;
    }

   // private PreparedStatement state = null;

    private String query = "INSERT INTO Thread(title, isClosed, isDeleted, date, message, slug, forum_ID, user_ID, user_email, forum_short) VALUES (?,?,?,?,?,?,?,?,?,?)";
    //private SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");

        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String forum = null;
        String title = null;
        String user = null;
        String dateString = null;
        String message = null;
        String slug = null;
        java.util.Date date = null;
        Boolean is_Closed = null;
        //-----
        Boolean is_Deleted = null;
        SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            forum = requestJson.get("forum").getAsString();
            title = requestJson.get("title").getAsString();
            user = requestJson.get("user").getAsString();
            dateString = requestJson.get("date").getAsString();
            message = requestJson.get("message").getAsString();
            slug = requestJson.get("slug").getAsString();
            is_Closed = requestJson.get("isClosed").getAsBoolean();
            System.out.println(dateString);
            date = SDF.parse(dateString);

        } catch (Exception e) {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }


        JsonObject detailsUser = null;
        JsonObject detailsForum = null;

        JsonElement is_Del = requestJson.get("isDeleted");
        is_Deleted = false;
        if(is_Del!=null)
            is_Deleted = is_Del.getAsBoolean();

        try (Connection connection = DB.getConnection()) {


            detailsUser = DetailsUser.getDetails(user, connection);
            detailsForum = DetailsForum.getDetails(forum, false,connection);

            PreparedStatement state = connection.prepareStatement(query);

            if (detailsForum == null || detailsUser == null) {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
                return;
            }

            state.setString(1, title);
            state.setBoolean(2, is_Closed);
            state.setBoolean(3, is_Deleted);
            state.setString(4, SDF.format(date));
            state.setString(5, message);
            state.setString(6, slug);
            state.setInt(7, detailsForum.get("id").getAsInt());
            state.setInt(8, detailsUser.get("id").getAsInt());
            state.setString(10, detailsForum.get("short_name").getAsString());
            state.setString(9,detailsUser.get("email").getAsString());

            try {
                state.executeUpdate();
            }
            catch (MySQLIntegrityConstraintViolationException ignore)
            {
                   ignore.printStackTrace();
               // return;
            }
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                JsonObject responseJSON = DetailsThread.getDetails(title, false, false, connection);

//                responseJSON.addProperty("date", SDF.format(date));
//                responseJSON.addProperty("forum", forum);
//                responseJSON.addProperty("id", rs.getInt(1));
//                responseJSON.addProperty("isClosed", is_Closed);
//                responseJSON.addProperty("isDeleted", is_Deleted);
//                responseJSON.addProperty("message", message);
//                responseJSON.addProperty("dislikes", 0);
//                responseJSON.addProperty("likes", 0);
//                responseJSON.addProperty("points", 0);
//                responseJSON.addProperty("posts", 0);
//                responseJSON.addProperty("slug", slug);
//                responseJSON.addProperty("title", title);
//                responseJSON.addProperty("user", user);


                result.add("response", responseJSON);
                connection.close();
                response.getWriter().println(result);

        } catch (SQLException e) {
            e.printStackTrace();
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }


    }
}