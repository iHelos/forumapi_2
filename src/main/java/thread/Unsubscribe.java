package thread;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 16.10.15.
 */
public class Unsubscribe extends HttpServlet {
    @NotNull private Database DB;
  //  @NotNull private Connection connection;

    public Unsubscribe (Database mainConnect)
    {
        DB = mainConnect;
    }

    //private PreparedStatement state = null;
    private String query = "DELETE Subscriptions FROM Subscriptions left join User on User.ID_user = user_id LEFT JOIN Thread on Thread.ID_thread = thread_id where User.email = ? and ID_thread = ?";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String user = null;
        Integer thread = null;

        try {
            user = requestJson.get("user").getAsString();
            thread = requestJson.get("thread").getAsInt();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try (Connection connection = DB.getConnection()) {

            PreparedStatement state = connection.prepareStatement(query);
            state.setString(1, user);
            state.setInt(2, thread);
            int marker = state.executeUpdate();
            connection.close();
            if (marker > 0) {
                JsonObject responseJSON = new JsonObject();
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                responseJSON.addProperty("thread", thread);
                responseJSON.addProperty("user", user);
                result.add("response", responseJSON);
                response.getWriter().println(result);
            }
            else {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
            }
        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(5));
        }

    }
}
