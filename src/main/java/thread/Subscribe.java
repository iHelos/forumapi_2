package thread;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

/**
 * Created by olegermakov on 16.10.15.
 */
public class Subscribe extends HttpServlet {
    @NotNull private Database DB;
   // @NotNull private Connection connection;

    public Subscribe (Database mainConnect)
    {
        DB = mainConnect;
    }

   // private PreparedStatement state = null;
    private String query = "INSERT INTO Subscriptions(user_id, thread_id) SELECT Sub.ID_user, Fee.ID_thread from User Sub join Thread Fee where Sub.email = ? AND Fee.ID_thread = ?";

    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        JsonObject requestJson = null;
        try {
            requestJson = new Gson().fromJson(request.getReader(), JsonObject.class);
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(3));
            return;
        }

        String user = null;
        Integer thread = null;

        try {
            user = requestJson.get("user").getAsString();
            thread = requestJson.get("thread").getAsInt();
        } catch (Exception e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        try (Connection connection = DB.getConnection();) {

           PreparedStatement state = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            state.setString(1, user);
            state.setInt(2, thread);
            int marker = state.executeUpdate();
            connection.close();
            if (marker > 0) {
                ResultSet rs = state.getGeneratedKeys();
                rs.next();
                JsonObject responseJSON = new JsonObject();
                JsonObject result = new JsonObject();
                result.addProperty("code", 0);
                responseJSON.addProperty("thread", thread);
                responseJSON.addProperty("user", user);
                result.add("response", responseJSON);
                response.getWriter().println(result);
            }
            else {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
            }
        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(5));
        }

    }

}
