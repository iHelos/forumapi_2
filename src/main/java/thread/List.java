package thread;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import forum.DetailsForum;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class List extends HttpServlet {
    @NotNull
    private Database DB;
  //  @NotNull private Connection connection;

    public List (Database mainConnect)
    {
        DB = mainConnect;
    }

   // private static PreparedStatement state = null;

    private static String queryCount = "SELECT count(*) FROM Post WHERE ThreadID = ? and isDeleted=false and isDeleted_thread = false";


    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String short_name = request.getParameter("forum");

        if (short_name != null && !short_name.equals(""))
        {
            String query = "SELECT * FROM ForumDBTest1.Thread left join Forum on Forum_ID = ID_forum where short_name = ?";

            String since_id_string = request.getParameter("since");
            if (since_id_string != null && !since_id_string.equals("")) {
                query += " and date >= '" + since_id_string + "'";
            }

            String order = request.getParameter("order");
            if (order == null || order.equals(""))
                order = "desc";
            else if (!order.equals("desc") && !order.equals("asc")) {
                response.getWriter().println(ErrorMsg.ReturnMsg(2));
                return;
            }

            query += " order by date " + order;

            String limit_string = request.getParameter("limit");
            if (limit_string != null && !limit_string.equals(""))
                query += " limit " + limit_string;

            try (Connection connection = DB.getConnection();)
            {
               PreparedStatement  state = connection.prepareStatement(query);

                state.setString(1, short_name);
                ResultSet result = state.executeQuery();
                JsonArray arrayThreads = new JsonArray();
                while (result.next()) {
                    JsonObject responseJSON = new JsonObject();
                    responseJSON.addProperty("id", result.getInt(1));
                    responseJSON.addProperty("title", result.getString(2));
                    responseJSON.addProperty("isClosed", result.getBoolean(3));
                    responseJSON.addProperty("isDeleted", result.getBoolean(4));
                    responseJSON.addProperty("date", result.getString(5).substring(0,19));
                    responseJSON.addProperty("message", result.getString(6));
                    responseJSON.addProperty("slug", result.getString(7));

                    int likes = result.getInt(10);
                    int dislikes = result.getInt(11);

                    responseJSON.addProperty("likes", likes);
                    responseJSON.addProperty("dislikes", dislikes);
                    responseJSON.addProperty("points", likes - dislikes);

                    JsonObject UserJS = DetailsUser.getDetails(result.getInt(9), connection);
                    JsonObject ForumJS = DetailsForum.getDetails(result.getInt(8), false,connection);

                    state = connection.prepareStatement(queryCount);
                    state.setInt(1, result.getInt(1));
                    ResultSet rs = state.executeQuery();
                    rs.next();
                    responseJSON.addProperty("posts", rs.getInt(1));

                    responseJSON.add("user", UserJS.get("email"));

                    responseJSON.add("forum", ForumJS.get("short_name"));
                    arrayThreads.add(responseJSON);
                }
                //connection.close();
                JsonObject resultJSON = new JsonObject();
                resultJSON.addProperty("code", 0);
                resultJSON.add("response", arrayThreads);
                response.getWriter().println(resultJSON);
                return;
            }
            catch (SQLException e)
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(4));
                return;
            }


        }

        String email = request.getParameter("user");
        if (email == null || email.equals(""))
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
            return;
        }

        ///-------------------------///

        String query = "SELECT * FROM ForumDBTest1.Thread left join User on User_ID = ID_user where email = ?";

        String since_id_string = request.getParameter("since");
        if (since_id_string != null && !since_id_string.equals("")) {
            query += " and date >= '" + since_id_string + "'";
        }

        String order = request.getParameter("order");
        if (order == null || order.equals(""))
            order = "desc";
        else if (!order.equals("desc") && !order.equals("asc")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        query += " order by date " + order;

        String limit_string = request.getParameter("limit");
        if (limit_string != null && !limit_string.equals(""))
            query += " limit " + limit_string;

        try (Connection connection = DB.getConnection();)
        {
           PreparedStatement  state = connection.prepareStatement(query);
            state.setString(1, email);
            ResultSet result = state.executeQuery();
            JsonArray arrayThreads = new JsonArray();
            while (result.next()) {
                JsonObject responseJSON = new JsonObject();
                responseJSON.addProperty("id", result.getInt(1));
                responseJSON.addProperty("title", result.getString(2));
                responseJSON.addProperty("isClosed", result.getBoolean(3));
                responseJSON.addProperty("isDeleted", result.getBoolean(4));
                responseJSON.addProperty("date", result.getString(5).substring(0,19));
                responseJSON.addProperty("message", result.getString(6));
                responseJSON.addProperty("slug", result.getString(7));

                int likes = result.getInt(10);
                int dislikes = result.getInt(11);

                responseJSON.addProperty("likes", likes);
                responseJSON.addProperty("dislikes", dislikes);
                responseJSON.addProperty("points", likes - dislikes);

//                JsonObject UserJS = DetailsUser.getDetails(result.getInt(9), connection);
//                JsonObject ForumJS = DetailsForum.getDetails(result.getInt(8), false,connection);

                responseJSON.addProperty("user", result.getString("user_email"));

                responseJSON.addProperty("forum", result.getString("forum_short"));

                state = connection.prepareStatement(queryCount);
                state.setInt(1, result.getInt(1));
                ResultSet rs = state.executeQuery();
                rs.next();
                responseJSON.addProperty("posts", rs.getInt(1));

                arrayThreads.add(responseJSON);
            }
           // connection.close();
            JsonObject resultJSON = new JsonObject();
            resultJSON.addProperty("code", 0);
            resultJSON.add("response", arrayThreads);
            response.getWriter().println(resultJSON);
            return;
        }
        catch (SQLException e)
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
            return;
        }
    }
}

