package thread;

import com.google.gson.JsonObject;
import forum.DetailsForum;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.DetailsUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by olegermakov on 16.10.15.
 */
public class DetailsThread extends HttpServlet {
    @NotNull
    private static Database DB;
   // @NotNull private Connection connection;

    public DetailsThread (Database mainConnect)
    {
        DB = mainConnect;
    }

  //  private static PreparedStatement state = null;

    private static String query = "SELECT * FROM Thread WHERE ID_thread = ?";
    private static String queryTitle = "SELECT * FROM Thread WHERE title = ?";
    private static String queryCount = "SELECT count(*) FROM Post WHERE ThreadID = ? and isDeleted=false and isDeleted_thread = false";

/*
##Arguments
###Optional
* related

   ```array``` include related entities. Possible values: ```['user', 'forum']```. Default: []


###Requried
* thread

   ```int``` thread id of this post


Requesting http://some.host.ru/db/api/thread/details/?thread=1:
```json
{
    "code": 0,
    "response": {
        "date": "2014-01-01 00:00:01",
        "dislikes": 0,
        "forum": "forum1",
        "id": 1,
        "isClosed": true,
        "isDeleted": true,
        "likes": 0,
        "message": "hey hey hey hey!",
        "points": 0,
        "posts": 0,
        "slug": "Threadwithsufficientlylargetitle",
        "title": "Thread With Sufficiently Large Title",
        "user": "example3@mail.ru"
    }
}
```

 */
    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String IDstring = request.getParameter("thread");
        if (IDstring == null || IDstring == "") {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }
        int ID = 0;
        try{
            ID = Integer.parseInt(IDstring);
        }
        catch (Exception e)
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String[] user = request.getParameterValues("related");
        Boolean markUser = false;
        Boolean markForum = false;

        if(user != null) {
            for (int i = 0; i < user.length; i++) {
                if (user[i].equals("user"))
                    markUser = true;
                else if (user[i].equals("forum"))
                    markForum = true;
                else {
                    response.getWriter().println(ErrorMsg.ReturnMsg(3));
                    return;
                }
            }
        }

        JsonObject result = new JsonObject();
        result.addProperty("code", 0);
        try (Connection connection = DB.getConnection()) {
            JsonObject responseJSON = getDetails(ID, markUser, markForum, connection);
            if (responseJSON == null) {
                response.getWriter().println(ErrorMsg.ReturnMsg(1));
                return;
            }
            result.add("response", responseJSON);
            response.setContentType("application/json; charset=utf-8");
            response.getWriter().println(result);
        }
        catch (SQLException e)
        {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
            return;
        }

    }

    public static JsonObject getDetails(int ID, Boolean User, Boolean Forum, Connection connection) throws SQLException {
       // Connection connection = DB.getConnection();
        PreparedStatement state = connection.prepareStatement(query);
        state.setInt(1, ID);
       // ResultSet result = state.executeQuery();
        return commonGetDetails(state, User, Forum, connection);
    }

    public static JsonObject getDetails(String title, Boolean User, Boolean Forum, Connection connection) throws SQLException {
        // Connection connection = DB.getConnection();
        PreparedStatement state = connection.prepareStatement(queryTitle);
        state.setString(1, title);
        //ResultSet result = state.executeQuery();
        return commonGetDetails(state, User, Forum, connection);
    }

    private static JsonObject commonGetDetails(PreparedStatement state, Boolean User, Boolean Forum, Connection con) throws SQLException {
        ResultSet result = state.executeQuery();
        JsonObject responseJSON = new JsonObject();
        if (result.next()) {
            responseJSON.addProperty("id", result.getInt(1));
            responseJSON.addProperty("title", result.getString(2));
            responseJSON.addProperty("isClosed", result.getBoolean(3));
            responseJSON.addProperty("isDeleted", result.getBoolean(4));
            responseJSON.addProperty("date", result.getString(5).substring(0,19));
            responseJSON.addProperty("message", result.getString(6));
            responseJSON.addProperty("slug", result.getString(7));

            int likes = result.getInt(10);
            int dislikes = result.getInt(11);

            responseJSON.addProperty("likes", likes);
            responseJSON.addProperty("dislikes", dislikes);
            responseJSON.addProperty("points", likes - dislikes);

            if(User) {
                JsonObject UserJS = DetailsUser.getDetails(result.getInt(9),con);
                responseJSON.add("user", UserJS);
            }
            else
                responseJSON.addProperty("user", result.getString("user_email"));

            if(Forum) {
                JsonObject ForumJS = DetailsForum.getDetails(result.getInt(8),false,con);
                responseJSON.add("forum", ForumJS);
            }
            else
                responseJSON.addProperty("forum", result.getString("forum_short"));

       //     if(result.getBoolean(4))
         //       responseJSON.addProperty("posts", 0);
           // else {

                state = con.prepareStatement(queryCount);
                state.setInt(1, result.getInt(1));
                ResultSet rs = state.executeQuery();
                rs.next();
                responseJSON.addProperty("posts", rs.getInt(1));
            //}
           // con.close();
        } else
            responseJSON = null;
        return responseJSON;
    }
}