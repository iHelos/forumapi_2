package thread;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import main.Database;
import main.ErrorMsg;
import org.jetbrains.annotations.NotNull;
import user.listFollowers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by olegermakov on 17.10.15.
 */
public class ListPosts extends HttpServlet {
    @NotNull private Database DB;
  //  @NotNull private Connection connection;

    public ListPosts (Database mainConnect)
    {
        DB = mainConnect;
    }
   // private PreparedStatement state = null;
    private static String query = "SELECT * FROM ForumDBTest1.Post where ThreadID = ? ";

            /*and FolloweeID >= ?  " +
            "order by FolloweeName ";*/


    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=utf-8");
        String threadStr = request.getParameter("thread");
        query = "SELECT * FROM ForumDBTest1.Post where ThreadID = ? ";
        int thread = 0;
        String since = null;
        if (threadStr == null || threadStr.equals("")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }
        else
        {
            try{
             thread = Integer.parseInt(threadStr);
            }
            catch (Exception e)
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(2));
                return;
            }
        }

        String since_id_string = request.getParameter("since");
        if (since_id_string != null && !since_id_string.equals("")) {
            query+= " and date >= '" + since_id_string +"'";
        }

        String order = request.getParameter("order");
        if (order == null || order.equals(""))
            order="desc";
        else if (!order.equals("desc") && !order.equals("asc")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        String sort = request.getParameter("sort");
        if (sort == null || sort.equals(""))
            sort="flat";
        else if (!sort.equals("flat") && !sort.equals("tree") && !sort.equals("parent_tree")) {
            response.getWriter().println(ErrorMsg.ReturnMsg(2));
            return;
        }

        if(sort!="flat")
            query += " order by PartialPath " + order + ", partialID " + order;
        else
            query += " order by date " + order;

        String limit_string = request.getParameter("limit");
        int limit = Integer.MAX_VALUE;
        if (limit_string != null && !limit_string.equals("")) {
            try{
                limit = Integer.parseInt(limit_string);
            }
            catch (Exception e)
            {
                response.getWriter().println(ErrorMsg.ReturnMsg(2));
                return;
            }
        }

        /*------------------------------------*/

        try (Connection connection = DB.getConnection();) {
           PreparedStatement state = connection.prepareStatement(query);
            state.setInt(1, thread);
            ResultSet result = state.executeQuery();
            JsonArray arrayPosts = new JsonArray();
            while(result.next() && limit>0)
            {
                JsonObject responseJSON = new JsonObject();
                int dislikes = result.getInt(3);
                int likes = result.getInt(6);
                int points = likes - dislikes;
                responseJSON.addProperty("id", result.getInt(1));
                responseJSON.addProperty("date", result.getString(2).substring(0, 19));
                responseJSON.addProperty("dislikes", dislikes);
                responseJSON.addProperty("isClosed", result.getBoolean(4));
                responseJSON.addProperty("isDeleted", result.getBoolean(5) || result.getBoolean("isDeleted_thread"));
                responseJSON.addProperty("likes", likes);
                responseJSON.addProperty("points", points);
                responseJSON.addProperty("message", result.getString(7));
                responseJSON.addProperty("isSpam", result.getBoolean(8));
                responseJSON.addProperty("isHighlighted", result.getBoolean(9));
                responseJSON.addProperty("isApproved", result.getBoolean(11));
                responseJSON.addProperty("isEdited", result.getBoolean(16));
                responseJSON.addProperty("user", result.getString("email_user"));
                responseJSON.addProperty("thread", thread);

//                state = connection.prepareStatement("select short_name from Forum where ID_forum = ?");
//                state.setInt(1, result.getInt("ForumID"));
//                ResultSet ForumRS = state.executeQuery();
//                ForumRS.next();
                responseJSON.addProperty("forum", result.getString("short_forum"));

//                state = connection.prepareStatement("select email from User where ID_user = ?");
//                state.setInt(1,result.getInt("User"));
//                ResultSet UserRS = state.executeQuery();
//                UserRS.next();


                String Parent = result.getString(12);
                Integer ParentID = null;
                if(Parent!=null)
                    ParentID = Integer.parseInt(Parent);
                responseJSON.addProperty("parent", ParentID);
                if (sort.equals("parent_tree")) {
                    if(ParentID == null)
                        limit--;
                }
                else
                {
                    limit--;
                }
                arrayPosts.add(responseJSON);
            }
            //connection.close();

            JsonObject resultJSON = new JsonObject();
            resultJSON.addProperty("code",0);
            resultJSON.add("response", arrayPosts);
            response.getWriter().println(resultJSON);

        }
        catch (SQLException e) {
            response.getWriter().println(ErrorMsg.ReturnMsg(4));
        }
    }
}
